#include QMK_KEYBOARD_H
#include "action_layer.h"
#include "version.h"
#include "timer.h"

// layer definitions
enum layers {
    BASE = 0, // default layer

    GENG, // generic gaming layer
    GWRS, // guild wars 2
    GWUI, // guild wars 2 ui
    GWFN, // guild wars 2 function keys
    GWMN, // guild wars 2 mounts
    DIAB, // diablo 3

    SYMB, // symbols/numpad
    FUNK, // function keys
    MDIA, // media keys
    ARRW, // arrow keys
    EMJI, // emoji keys

    META, // jump to other base layers
};


// mouse speeds
#define MOUSE_SPEED_DEFAULT 1000l
#define MOUSE_SPEED_0 100l
#define MOUSE_SPEED_1 500l
#define MOUSE_SPEED_2 2000l

#define MOUSE_WHEEL_SPEED_DEFAULT 30l
#define MOUSE_WHEEL_SPEED_0 10l
#define MOUSE_WHEEL_SPEED_1 50l
#define MOUSE_WHEEL_SPEED_2 100l

#define TIME_BETWEEN_MOVEMENT_DEFAULT 1000000l / MOUSE_SPEED_DEFAULT
#define TIME_BETWEEN_MOVEMENT_0 1000000l / MOUSE_SPEED_0
#define TIME_BETWEEN_MOVEMENT_1 1000000l / MOUSE_SPEED_1
#define TIME_BETWEEN_MOVEMENT_2 1000000l / MOUSE_SPEED_2

#define TIME_BETWEEN_WHEEL_MOVEMENT_DEFAULT 1000000l / MOUSE_WHEEL_SPEED_DEFAULT
#define TIME_BETWEEN_WHEEL_MOVEMENT_0 1000000l / MOUSE_WHEEL_SPEED_0
#define TIME_BETWEEN_WHEEL_MOVEMENT_1 1000000l / MOUSE_WHEEL_SPEED_1
#define TIME_BETWEEN_WHEEL_MOVEMENT_2 1000000l / MOUSE_WHEEL_SPEED_2

struct MouseState {
  bool up;
  bool down;
  bool left;
  bool right;
  bool wheel_up;
  bool wheel_down;
  bool wheel_left;
  bool wheel_right;
  bool button_left;
  bool button_right;
  bool button_middle;
  uint32_t time_between_movement;
  uint32_t time_between_wheel_movement;
  uint32_t last_movement_time;
  uint32_t last_wheel_movement_time;
} mouse_state = {
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  TIME_BETWEEN_MOVEMENT_DEFAULT,
  TIME_BETWEEN_WHEEL_MOVEMENT_DEFAULT,
  0,
  0,
};

enum custom_keycodes {
  // prints current unicode input mode
  KC_UC_IN_MODE = SAFE_RANGE,
  // windows navigation
  KC_ALT_TAB,
  KC_ALT_SFT_TAB,
  // tab navigation
  KC_SFT_TAB,
  KC_CTRL_TAB,
  KC_CTRL_SFT_TAB,
  KC_CTRL_PG_DN,
  KC_CTRL_PG_UP,
  KC_CTRL_W,
  // tmux
  KC_TMUX_PREFIX,
  // symbols
  KC_AUML, // ä
  KC_OUML, // ö
  KC_UUML, // ü
  KC_ASCII_LENNY_FACE, // ( ͡° ͜ʖ ͡°)
  KC_ASCII_SHRUG, // ¯\_(ツ)_/¯
  KC_ASCII_ANGRY, // ಠ_ಠ
  KC_ASCII_THROW_TABLE, // (╯°□°）╯︵ ┻━┻
  // mouse
  KC_MOUSE_UP,
  KC_MOUSE_DOWN,
  KC_MOUSE_LEFT,
  KC_MOUSE_RIGHT,
  KC_MOUSE_WHEEL_UP,
  KC_MOUSE_WHEEL_DOWN,
  KC_MOUSE_WHEEL_LEFT,
  KC_MOUSE_WHEEL_RIGHT,
  KC_MOUSE_BUTTON_LEFT,
  KC_MOUSE_BUTTON_RIGHT,
  KC_MOUSE_BUTTON_MIDDLE,
  KC_MOUSE_SPEED_0,
  KC_MOUSE_SPEED_1,
  KC_MOUSE_SPEED_2,
  KC_MOUSE_SPEED_3,
  // gw2
  KC_GW2_DODGEJUMP,
  // where dynamic macros start
  DYNAMIC_MACRO_RANGE,
};

enum unicode_keycodes {
  // emoji
  UC_EMOJI_FACE_WITH_TEARS_OF_JOY, // 😂
  UC_EMOJI_SMILING_FACE_WITH_SUNGLASSES, // 😎
  UC_EMOJI_THINKING_FACE, // 🤔
  UC_EMOJI_PERSON_SHRUGGING, // 🤷
  UC_EMOJI_DROOLING_FACE, // 🤤
  UC_EMOJI_CONFUSED_FACE, // 😕
  UC_EMOJI_GRINNING_FACE_WITH_SWEAT, // 😅
  UC_EMOJI_FACE_WITH_TONGUE, // 😛
  UC_EMOJI_MONEY_MOUTH_FACE, // 🤑
  UC_EMOJI_SLEEPING_FACE, // 😴
  UC_EMOJI_CRYING_FACE, // 😢
  UC_EMOJI_LOUDLY_CRYING_FACE, // 😭
  UC_EMOJI_THUMBS_UP, // 👍
  UC_EMOJI_THUMBS_DOWN, // 👎
  UC_EMOJI_RED_HEART, // ️❤️
  UC_EMOJI_OK_HAND, // 👌
  UC_EMOJI_PILE_OF_POO, // 💩
  UC_EMOJI_FACE_VOMITING, // 🤮
  UC_EMOJI_SEE_NO_EVIL_MONKEY, // 🙈
  UC_EMOJI_HEAR_NO_EVIL_MONKEY, // 🙉
  UC_EMOJI_SPEAK_NO_EVIL_MONKEY, // 🙊
  UC_EMOJI_CHECK, // ✓
  UC_EURO, // €
  UC_SSS, // ß
};

const uint32_t PROGMEM unicode_map[] = {
  [UC_EMOJI_FACE_WITH_TEARS_OF_JOY] = 0x1F602, // 😂
  [UC_EMOJI_SMILING_FACE_WITH_SUNGLASSES] = 0x1F60E, // 😎
  [UC_EMOJI_THINKING_FACE] = 0x1F914, // 🤔
  [UC_EMOJI_PERSON_SHRUGGING] = 0x1F937, // 🤷
  [UC_EMOJI_DROOLING_FACE] = 0x1F924, // 🤤
  [UC_EMOJI_CONFUSED_FACE] = 0x1F615, // 😕
  [UC_EMOJI_GRINNING_FACE_WITH_SWEAT] = 0x1F605, // 😅
  [UC_EMOJI_FACE_WITH_TONGUE] = 0x1F61B, // 😛
  [UC_EMOJI_MONEY_MOUTH_FACE] = 0x1F911, // 🤑
  [UC_EMOJI_SLEEPING_FACE] = 0x1F634, // 😴
  [UC_EMOJI_CRYING_FACE] =0x1F622, // 😢
  [UC_EMOJI_LOUDLY_CRYING_FACE] = 0x1F62D, // 😭
  [UC_EMOJI_THUMBS_UP] = 0x1F44D, // 👍
  [UC_EMOJI_THUMBS_DOWN] = 0x1F44E, // 👎
  [UC_EMOJI_RED_HEART] = 0xFE0F, // ️❤️
  [UC_EMOJI_OK_HAND] = 0x1F44C, // 👌
  [UC_EMOJI_PILE_OF_POO] = 0x1F4A9, // 💩
  [UC_EMOJI_FACE_VOMITING] = 0x1F92E, // 🤮
  [UC_EMOJI_SEE_NO_EVIL_MONKEY] = 0x1F648, // 🙈
  [UC_EMOJI_HEAR_NO_EVIL_MONKEY] = 0x1F649, // 🙉
  [UC_EMOJI_SPEAK_NO_EVIL_MONKEY] = 0x1F64A, // 🙊
  [UC_EMOJI_CHECK] = 0x2713, // ✓
  [UC_EURO] = 0x20AC, // €
  [UC_SSS] = 0xDF, // ß
};

#include "dynamic_macro.h"

// d3 macro to spam key in 'tact'
bool spam_d3_kc2 = false;

// to reset all modifiers if a ctrl/alt + key macro was used and we go back to base layer
bool macro_used = false;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Basic layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |  EMJI  |   1  |   2  |   3  |   4  |   5  | Pause|           |SclLck|   6  |   7  |   8  |   9  |   0  | META   |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | App    |   Q  |   W  |   E  |   R  |   T  | LOCK |           | caps |   Y  |   U  |   I  |   O  | Alt/P|   \    |
 * |--------+------+------+------+------+------|      |           | lock |------+------+------+------+------+--------|
 * |BkSp/Sft| Alt/A|   S  |   D  |   F  |   G  |------|           |------|   H  |   J  |   K  |   L  | Alt/;| '/Sft  |
 * |--------+------+------+------+------+------| Ply1 |           | Ins  |------+------+------+------+------+--------|
 * | Esc/Cmd|Ctrl/Z|Sft/X |   C  |   V  |   B  |      |           |      |   N  |   M  |   ,  | Sft/.|-/Ctrl| //Cmd  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |Grv/L1| FUNK | MDIA | SYMB | ARRW |                                       | ARRW | SYMB | MDIA | FUNK | tmux |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,---------------.
 *                                        |      |      |       |EEPRST| RESET  |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |UCINMD|       | SYMB |  SYMB/ |      |
 *                                 | Space| MDIA |------|       |------|  Tab   |Enter |
 *                                 |      |      | MDIA |       | BASE |        |      |
 *                                 `--------------------'       `----------------------'
 */
[BASE] = LAYOUT_ergodox(
    // left hand
    OSL(EMJI),        KC_1,         KC_2,         KC_3,     KC_4,     KC_5,     KC_PAUSE,
    KC_APP,           KC_Q,         KC_W,         KC_E,     KC_R,     KC_T,     KC_CAPS_LOCK,
    LSFT_T(KC_BSPC),  ALT_T(KC_A),  KC_S,         KC_D,     KC_F,     KC_G,
    LGUI_T(KC_ESC),   LCTL_T(KC_Z), LSFT_T(KC_X), KC_C,     KC_V,     KC_B,     QK_DYNAMIC_MACRO_PLAY_1,
    LT(SYMB,KC_GRV),  MO(FUNK),     MO(MDIA),     MO(SYMB), MO(ARRW),
                                                                      KC_TRNS,  KC_TRNS,
                                                                                KC_UC_IN_MODE,
                                                            KC_SPC,   MO(MDIA), TG(MDIA),
    // right hand
    KC_SCROLL_LOCK,   KC_6,            KC_7,     KC_8,     KC_9,           KC_0,            OSL(META),
    KC_CAPS,          KC_Y,            KC_U,     KC_I,     KC_O,           RALT_T(KC_P),    KC_BSLS,
                      KC_H,            KC_J,     KC_K,     KC_L,           LALT_T(KC_SCLN), RSFT_T(KC_QUOT),
    KC_INSERT,        KC_N,            KC_M,     KC_COMM,  RSFT_T(KC_DOT), RCTL_T(KC_MINS), RGUI_T(KC_SLSH),
                                       TT(ARRW), TT(SYMB), TT(MDIA),       TT(FUNK),        KC_TMUX_PREFIX,
    KC_TRNS,          KC_TRNS,
    TG(SYMB),
    TO(BASE),         LT(SYMB,KC_TAB), KC_ENT
),
/* Generic gaming layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * | Esc    |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | Tab    |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | Caps   |   A  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LShift |   Z  |   X  |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | LCtrl| LAlt |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |        |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |      |       |      |        |      |
 *                                 |      |      |------|       |------|        |      |
 *                                 |      |      |      |       |      |        |      |
 *                                 `--------------------'       `----------------------'
 */
[GENG] = LAYOUT_ergodox(
    // left hand
    KC_ESC,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TAB,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_CAPS, KC_A,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_LSFT, KC_Z,    KC_X,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_LCTL, KC_LALT, KC_TRNS, KC_TRNS, KC_TRNS,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Guild wars 2 layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * | `      |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | Tab    |      |      |      |      |      |  UI  |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | GWFN   |   A  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|  M   |           |      |------+------+------+------+------+--------|
 * | Shift  |   Z  |   X  |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   | Ctrl | Alt  | GWMN |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |       |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |      |       |      |        |      |
 *                                 |      | ESC  |------|       |------|        |      |
 *                                 |      |      |   I  |       |      |        |      |
 *                                 `--------------------'       `----------------------'
 */
[GWRS] = LAYOUT_ergodox(
    // left hand
    KC_GRV,   KC_TRNS, KC_TRNS,   KC_TRNS, KC_TRNS,          KC_TRNS, KC_TRNS,
    KC_TAB,   KC_TRNS, KC_TRNS,   KC_TRNS, KC_TRNS,          KC_TRNS, TG(GWUI),
    MO(GWFN), KC_A,    KC_TRNS,   KC_TRNS, KC_TRNS,          KC_TRNS,
    KC_LSFT,  KC_Z,    KC_X,      KC_TRNS, KC_TRNS,          KC_TRNS, KC_M,
    KC_LCTL,  KC_LALT, OSL(GWMN), KC_TRNS, KC_TRNS,
                                                             KC_TRNS, KC_TRNS,
                                                                      KC_TRNS,
                                                    KC_TRNS, KC_ESC,  KC_I,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Guild wars 2 ui layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | O      |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | I      |      |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |   H  |   Z  |   M  |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[GWUI] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_O,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_I,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_H,    KC_Z,    KC_M,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Guild wars 2 function keys
 *,
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |  F1  |  F2  |  F3  |  F4  |  F5  |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |  C1  |  C2  |  C3  |  C4  |  C5  |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |  A1  |  A2  |  A3  |  A4  |  A5  |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |  MM  |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[GWFN] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_F1,         KC_F2,         KC_F3,         KC_F4,         KC_F5,         KC_TRNS,
    KC_TRNS, LCTL(KC_KP_1), LCTL(KC_KP_2), LCTL(KC_KP_3), LCTL(KC_KP_4), LCTL(KC_KP_5), KC_TRNS,
    KC_TRNS, LALT(KC_KP_1), LALT(KC_KP_2), LALT(KC_KP_3), LALT(KC_KP_4), LALT(KC_KP_5),
    KC_TRNS, KC_TRNS,       KC_TRNS,       KC_TRNS,       KC_TRNS,       KC_TRNS,       KC_TRNS,
    KC_TRNS, KC_TRNS,       KC_TRNS,       KC_TRNS,       KC_BTN3,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Guild wars 2 mount layer
 *,
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   4  |   5  |   6  |   7  |   8  |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |   9  |   3  |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[GWMN] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_KP_4, KC_KP_5, KC_KP_6, KC_KP_7, KC_KP_8, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_KP_9, KC_KP_3,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Diablo 3 layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * | Esc    |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | Tab    |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | I      |   A  |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LShift |   Z  |   X  |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |        |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |      |       |      |        |      |
 *                                 |      |      |------|       |------|        |      |
 *                                 |      |      |      |       |      |        |      |
 *                                 `--------------------'       `----------------------'
 */
[DIAB] = LAYOUT_ergodox(
    // left hand
    KC_ESC,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TAB,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_I,    KC_A,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_LSFT, KC_Z,    KC_X,    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Symbol layer
 *
 * ,---------------------------------------------------.           ,--------------------------------------------------.
 * |         |   1  |   2  |   3  |   4  |   5  | Stop |           |NumLck|   6  |   7  |   8  |   9  |   0  |  BASE  |
 * |---------+------+------+------+------+------+------|           |------+------+------+------+------+------+--------|
 * |   &     |   !  |   @  |   (  |   )  |   |  | Rec1 |           | Rec2 |   *  |   7  |   8  |   9  |   /  |   ,    |
 * |---------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |         |   #  |   $  |   [  |   ]  |   `  |------|           |------|   €  |   4  |   5  |   6  |   0  |   .    |
 * |---------+------+------+------+------+------| Ply1 |           | Ply2 |------+------+------+------+------+--------|
 * |   =     |   %  |   ^  |   {  |   }  |   ~  |      |           |      |   +  |   1  |   2  |   3  |   -  |   =    |
 * `---------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |       | FUNK | MDIA | SYMB | ARRW |                                       | A_Uml| O_Uml| U_Uml| SSS  | Print|
 *   `-----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 | Space| MDIA |------|       |------| Tab  |Enter |
 *                                 |      |      | BASE |       | MDIA |      |      |
 *                                 `--------------------'       `--------------------'
 */
[SYMB] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     QK_DYNAMIC_MACRO_RECORD_STOP,
    KC_AMPR, KC_EXLM,  KC_AT,    KC_LPRN,  KC_RPRN,  KC_PIPE,  QK_DYNAMIC_MACRO_RECORD_START_1,
    KC_TRNS, KC_HASH,  KC_DLR,   KC_LBRC,  KC_RBRC,  KC_GRV,
    KC_EQL,  KC_PERC,  KC_CIRC,  KC_LCBR,  KC_RCBR,  KC_TILD,  QK_DYNAMIC_MACRO_PLAY_1,
    KC_TRNS, MO(FUNK), MO(MDIA), MO(SYMB), MO(ARRW),
                                                     KC_TRNS,  KC_TRNS,
                                                               KC_TRNS,
                                           KC_SPC,   MO(MDIA), TO(BASE),
    // right hand
    KC_NUM_LOCK,                         KC_6,       KC_7,    KC_8,    KC_9,    KC_0,      TO(BASE),
    QK_DYNAMIC_MACRO_RECORD_START_1, KC_PAST,    KC_KP_7, KC_KP_8, KC_KP_9, KC_PSLS,   KC_PCMM,
                                     X(UC_EURO), KC_KP_4, KC_KP_5, KC_KP_6, KC_KP_0,   KC_PDOT,
    QK_DYNAMIC_MACRO_PLAY_2,         KC_PPLS,    KC_KP_1, KC_KP_2, KC_KP_3, KC_PMNS,   KC_PEQL,
                                                 KC_AUML, KC_OUML, KC_UUML, X(UC_SSS), KC_PSCR,
    KC_TRNS,                         KC_TRNS,
    KC_TRNS,
    TG(MDIA),                        KC_TAB,     KC_PENT
),
/* function keys layer
 *
 * ,---------------------------------------------------.           ,--------------------------------------------------.
 * |         |      |      |      |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |---------+------+------+------+------+------+------|           |------+------+------+------+------+------+--------|
 * |         |      | F19  | F20  | F21  | F24  |      |           |      |  F12 |  F7  |  F8  |  F9  |      |        |
 * |---------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |         |      | F16  | F17  | F18  | F23  |------|           |------|  F11 |  F4  |  F5  |  F6  |      |        |
 * |---------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |         |      | F13  | F14  | F15  | F22  |      |           |      |  F10 |  F1  |  F2  |  F3  |      |        |
 * `---------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |       |      |      |      |      |                                       |      |      |      |      |      |
 *   `-----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[FUNK] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_F19,  KC_F20,  KC_F21,  KC_F24,  KC_TRNS,
    KC_TRNS, KC_TRNS, KC_F16,  KC_F17,  KC_F18,  KC_F23,
    KC_TRNS, KC_TRNS, KC_F13,  KC_F14,  KC_F15,  KC_F22,  KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS, KC_F12,  KC_F7,   KC_F8,   KC_F9,   KC_TRNS, KC_TRNS,
             KC_F11,  KC_F4,   KC_F5,   KC_F6,   KC_TRNS, KC_TRNS,
    KC_TRNS, KC_F10,  KC_F1,   KC_F2,   KC_F3,   KC_TRNS, KC_TRNS,
                      KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* Media and mouse keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |WhlUp |      |      |      |           |      |      |      | MsUp |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |   S1   |  S2  |WhLft |WhlDn |WhRght|      |      |           |      |brgtup|MsLeft|MsDown|MsRght|  S2  |   S1   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |   S0   |      |  S0  |  S1  |  S2  |      |------|           |------|brgtdn|  S0  |  S1  |  S2  |      |   S0   |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        | rwnd | Prev | Play | Next | ffwd |      |           |      | rwnd | Prev | Play | Next | ffwd |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |VolDn | Mute |VolUp |                                       |VolDn | Mute |VolUp |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      | Rclk | Lclk |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      | BASE |       | Mclk |      |      |
 *                                 `--------------------'       `--------------------'
 */
[MDIA] = LAYOUT_ergodox(
    // left hand
    KC_TRNS,          KC_TRNS,          KC_TRNS,             KC_MOUSE_WHEEL_UP,   KC_TRNS,              KC_TRNS,          KC_TRNS,
    KC_MOUSE_SPEED_1, KC_MOUSE_SPEED_2, KC_MOUSE_WHEEL_LEFT, KC_MOUSE_WHEEL_DOWN, KC_MOUSE_WHEEL_RIGHT, KC_TRNS,          KC_TRNS,
    KC_MOUSE_SPEED_0, KC_TRNS,          KC_MOUSE_SPEED_0,    KC_MOUSE_SPEED_1,    KC_MOUSE_SPEED_2,     KC_TRNS,
    KC_TRNS,          LALT(LCTL(KC_1)), LALT(LCTL(KC_2)),    LALT(LCTL(KC_3)),    LALT(LCTL(KC_4)),     LALT(LCTL(KC_5)), KC_TRNS,
    KC_TRNS,          KC_TRNS,          LALT(LCTL(KC_6)),    LALT(LCTL(KC_7)),    LALT(LCTL(KC_8)),
                                                                                                        KC_TRNS,          KC_TRNS,
                                                                                                                          KC_TRNS,
                                                                                  KC_TRNS,              KC_TRNS,          TO(BASE),
    // right hand
    KC_TRNS,                KC_TRNS,              KC_TRNS,          KC_MOUSE_UP,      KC_TRNS,          KC_TRNS,          TO(BASE),
    KC_TRNS,                KC_BRIU,              KC_MOUSE_LEFT,    KC_MOUSE_DOWN,    KC_MOUSE_RIGHT,   KC_MOUSE_SPEED_2, KC_MOUSE_SPEED_1,
                            KC_BRID,              KC_MOUSE_SPEED_0, KC_MOUSE_SPEED_1, KC_MOUSE_SPEED_2, KC_TRNS,          KC_MOUSE_SPEED_0,
    KC_TRNS,                KC_MRWD,              KC_MPRV,          KC_MPLY,          KC_MNXT,          KC_MFFD,          KC_TRNS,
                                                  KC_VOLD,          KC_MUTE,          KC_VOLU,          KC_TRNS,          KC_TRNS,
    KC_TRNS,                KC_TRNS,
    KC_TRNS,
    KC_MOUSE_BUTTON_MIDDLE, KC_MOUSE_BUTTON_LEFT, KC_MOUSE_BUTTON_RIGHT
),
/* arrow keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        | Calc |  PC  | Mail |      |      |      |           |      |      |      | CtlW |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |WinCls|Bckspc|  Up  |  DEL |      |      |           |      |      |CtlPgU| PgUp |CtlPgD|      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      | Left | Down | Right|      |------|           |------|      | Pos1 |PgDown| End  |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[ARRW] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_CALC, KC_MYCM, KC_MAIL, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_BSPC, KC_UP,   KC_DEL,  KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_LEFT, KC_DOWN, KC_RGHT, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                                      KC_TRNS, KC_TRNS,
                                                               KC_TRNS,
                                             KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS, KC_TRNS, KC_TRNS,       KC_CTRL_W, KC_TRNS,       KC_TRNS, TO(BASE),
    KC_TRNS, KC_TRNS, KC_CTRL_PG_UP, KC_PGUP,   KC_CTRL_PG_DN, KC_TRNS, KC_TRNS,
             KC_TRNS, KC_HOME,       KC_PGDN,   KC_END,        KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS,       KC_TRNS,   KC_TRNS,       KC_TRNS, KC_TRNS,
                      KC_TRNS,       KC_TRNS,   KC_TRNS,       KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
/* emoji keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |lenny |shrug |angry |thrtbl|      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |  😂  |  😎  |  🤔  |  🤷  |  🤤  |  😕  |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------|  😅  |  😛  |  🤑  |  😴  |  😢  |  😭  |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |  👍  |  👎  |  ❤️  |  👌  |  💩  |  🤮  |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |  🙈  |  🙉  |  🙊  |  ✓  |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[EMJI] = LAYOUT_ergodox(
    // left hand
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                                 KC_TRNS, KC_TRNS,
                                                          KC_TRNS,
                                        KC_TRNS, KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS, KC_ASCII_LENNY_FACE,                     KC_ASCII_SHRUG,                           KC_ASCII_ANGRY,                  KC_ASCII_THROW_TABLE,             KC_TRNS,                      TO(BASE),
    KC_TRNS, X(UC_EMOJI_FACE_WITH_TEARS_OF_JOY),      X(UC_EMOJI_SMILING_FACE_WITH_SUNGLASSES), X(UC_EMOJI_THINKING_FACE),       X(UC_EMOJI_PERSON_SHRUGGING),     X(UC_EMOJI_DROOLING_FACE),    X(UC_EMOJI_CONFUSED_FACE),
             X(UC_EMOJI_GRINNING_FACE_WITH_SWEAT),    X(UC_EMOJI_FACE_WITH_TONGUE),             X(UC_EMOJI_MONEY_MOUTH_FACE),    X(UC_EMOJI_SLEEPING_FACE),        X(UC_EMOJI_CRYING_FACE),      X(UC_EMOJI_LOUDLY_CRYING_FACE),
    KC_TRNS, X(UC_EMOJI_THUMBS_UP),                   X(UC_EMOJI_THUMBS_DOWN),                  X(UC_EMOJI_RED_HEART),           X(UC_EMOJI_OK_HAND),              X(UC_EMOJI_PILE_OF_POO),      X(UC_EMOJI_FACE_VOMITING),
                                                      X(UC_EMOJI_SEE_NO_EVIL_MONKEY),           X(UC_EMOJI_HEAR_NO_EVIL_MONKEY), X(UC_EMOJI_SPEAK_NO_EVIL_MONKEY), X(UC_EMOJI_CHECK), KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS,                                 KC_TRNS
),
/* meta navigation keys
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |  BASE  | GENG | GWRS | GENG |      |      |      |           |      |      |      |      |      |      |  BASE  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |UC OSX|UC LNX|UC WIN|UNWINC|      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |------|           |------|      |      |      |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |      |
 *                                 ,------|------|------|       |------+------+------.
 *                                 |      |      |      |       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |      |       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[META] = LAYOUT_ergodox(
    TO(BASE), TO(GENG),               TO(GWRS),               TO(DIAB),                 KC_TRNS,                    KC_TRNS, KC_TRNS,
    KC_TRNS,  QK_UNICODE_MODE_MACOS,  QK_UNICODE_MODE_LINUX,  QK_UNICODE_MODE_WINDOWS,  QK_UNICODE_MODE_WINCOMPOSE, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS,                KC_TRNS,                KC_TRNS,                  KC_TRNS,                    KC_TRNS,
    KC_TRNS,  KC_TRNS,                KC_TRNS,                KC_TRNS,                  KC_TRNS,                    KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS,                KC_TRNS,                KC_TRNS,                  KC_TRNS,
                                                                                                                    KC_TRNS, KC_TRNS,
                                                                                                                             KC_TRNS,
                                                                                        KC_TRNS,                    KC_TRNS, KC_TRNS,
    // right hand
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, TO(BASE),
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
              KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
    KC_TRNS, KC_TRNS,
    KC_TRNS,
    KC_TRNS, KC_TRNS, KC_TRNS
),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (!process_record_dynamic_macro(keycode, record)) {
    return false;
  }

  // reset time for mouse
  if (keycode >= KC_MOUSE_UP && keycode <= KC_MOUSE_RIGHT) {
    if (!mouse_state.up && !mouse_state.down && !mouse_state.left && !mouse_state.right) {
      mouse_state.last_movement_time = timer_read32();
    }
  }
  if (keycode >= KC_MOUSE_WHEEL_UP && keycode <= KC_MOUSE_WHEEL_RIGHT) {
    if (!mouse_state.wheel_up && !mouse_state.wheel_down && !mouse_state.wheel_left && !mouse_state.wheel_right) {
      mouse_state.last_wheel_movement_time = timer_read32();
    }
  }

  // number of pressed speed buttons
  static int mouse_speed_buttons_pressed = 0;

  switch (keycode) {
    // spam keycode
    case KC_2: {
      uint8_t layer = biton32(layer_state);
      // diablo 3
      if (record->event.pressed && layer == DIAB) {
        spam_d3_kc2 = true;
      } else {
        spam_d3_kc2 = false;
      }
      break;
    }
    case KC_UC_IN_MODE:
      if (!record->event.pressed) {
        // TODO: BUG: change unicode mode with UC_M_OS then printing with KC_UC_IN_MODE will result in printing twice
        switch(get_unicode_input_mode()) {
          case UNICODE_MODE_MACOS:
            SEND_STRING("OSX");
            return false;
          case UNICODE_MODE_LINUX:
            SEND_STRING("LNX");
            return false;
          case UNICODE_MODE_WINDOWS:
            SEND_STRING("WIN");
            return false;
          case UNICODE_MODE_WINCOMPOSE:
            SEND_STRING("WINC");
            return false;
        }
      }
      break;
    case QK_UNICODE_MODE_MACOS:
    case QK_UNICODE_MODE_LINUX:
    case QK_UNICODE_MODE_WINDOWS:
    case QK_UNICODE_MODE_WINCOMPOSE:
      process_unicode_common(keycode, record);
      layer_state_set(BASE);
      return false;
    case KC_ALT_TAB:
      if (record->event.pressed) SEND_STRING(SS_LALT(SS_TAP(X_TAB)));
      macro_used = true;
      return false;
    case KC_ALT_SFT_TAB:
      if (record->event.pressed) SEND_STRING(SS_LSFT(SS_TAP(X_TAB)));
      macro_used = true;
      return false;
    case KC_SFT_TAB:
      if (record->event.pressed) SEND_STRING(SS_LSFT(SS_TAP(X_TAB)));
      return false;
    case KC_CTRL_TAB:
      if (record->event.pressed) SEND_STRING(SS_LCTL(SS_TAP(X_TAB)));
      macro_used = true;
      return false;
    case KC_CTRL_SFT_TAB:
      if (record->event.pressed) SEND_STRING(SS_LCTL(SS_LSFT(SS_TAP(X_TAB))));
      macro_used = true;
      return false;
    case KC_CTRL_PG_DN:
      if (record->event.pressed) SEND_STRING(SS_LCTL(SS_TAP(X_PGDN)));
      return false;
    case KC_CTRL_PG_UP:
      if (record->event.pressed) SEND_STRING(SS_LCTL(SS_TAP(X_PGUP)));
      return false;
    case KC_CTRL_W:
      if (record->event.pressed) SEND_STRING(SS_LCTL(SS_TAP(X_W)));
      return false;
    case KC_TMUX_PREFIX:
        if (record->event.pressed) SEND_STRING(SS_LCTL(SS_TAP(X_A)));
        return false;
    case KC_AUML:
      if (record->event.pressed) {
        if (keyboard_report->mods & MOD_BIT(KC_LSFT) ||
            keyboard_report->mods & MOD_BIT(KC_RSFT)) {
          send_unicode_string("Ä");
        } else {
          send_unicode_string("ä");
        }
      }
      return false;
    case KC_OUML:
      if (record->event.pressed) {
        if (keyboard_report->mods & MOD_BIT(KC_LSFT) ||
            keyboard_report->mods & MOD_BIT(KC_RSFT)) {
          send_unicode_string("Ö");
        } else {
          send_unicode_string("ö");
        }
      }
      return false;
    case KC_UUML:
      if (record->event.pressed) {
        if (keyboard_report->mods & MOD_BIT(KC_LSFT) ||
            keyboard_report->mods & MOD_BIT(KC_RSFT)) {
          send_unicode_string("Ü");
        } else {
          send_unicode_string("ü");
        }
      }
      return false;
    case KC_ASCII_LENNY_FACE:
      if (record->event.pressed) {
        send_unicode_string("( ͡° ͜ʖ ͡°)");
      }
      return true;
    case KC_ASCII_SHRUG:
      if (record->event.pressed) {
        send_unicode_string("¯\\_(ツ)_/¯");
      }
      return true;
    case KC_ASCII_ANGRY:
      if (record->event.pressed) {
        send_unicode_string("ヽ(ಠ_ಠ)ノ");
      }
      return true;
    case KC_ASCII_THROW_TABLE:
      if (record->event.pressed) {
        send_unicode_string("(╯°□°)╯︵ ┻━┻");
      }
      return true;
    case KC_MOUSE_UP:
      mouse_state.up = record->event.pressed;
      break;
    case KC_MOUSE_DOWN:
      mouse_state.down = record->event.pressed;
      break;
    case KC_MOUSE_LEFT:
      mouse_state.left = record->event.pressed;
      break;
    case KC_MOUSE_RIGHT:
      mouse_state.right = record->event.pressed;
      break;
    case KC_MOUSE_WHEEL_UP:
      mouse_state.wheel_up = record->event.pressed;
      break;
    case KC_MOUSE_WHEEL_DOWN:
      mouse_state.wheel_down = record->event.pressed;
      break;
    case KC_MOUSE_WHEEL_LEFT:
      mouse_state.wheel_left = record->event.pressed;
      break;
    case KC_MOUSE_WHEEL_RIGHT:
      mouse_state.wheel_right = record->event.pressed;
      break;
    case KC_MOUSE_BUTTON_LEFT:
      mouse_state.button_left = record->event.pressed;
      break;
    case KC_MOUSE_BUTTON_RIGHT:
      mouse_state.button_right = record->event.pressed;
      break;
    case KC_MOUSE_BUTTON_MIDDLE:
      mouse_state.button_middle = record->event.pressed;
      break;
    case KC_MOUSE_SPEED_0:
      if (record->event.pressed) {
        mouse_state.time_between_movement = TIME_BETWEEN_MOVEMENT_0;
        mouse_state.time_between_wheel_movement = TIME_BETWEEN_WHEEL_MOVEMENT_0;
        mouse_speed_buttons_pressed++;
      } else {
        mouse_speed_buttons_pressed--;
      }
      break;
    case KC_MOUSE_SPEED_1:
      if (record->event.pressed) {
        mouse_state.time_between_movement = TIME_BETWEEN_MOVEMENT_1;
        mouse_state.time_between_wheel_movement = TIME_BETWEEN_WHEEL_MOVEMENT_1;
        mouse_speed_buttons_pressed++;
      } else {
        mouse_speed_buttons_pressed--;
      }
      break;
    case KC_MOUSE_SPEED_2:
      if (record->event.pressed) {
        mouse_state.time_between_movement = TIME_BETWEEN_MOVEMENT_2;
        mouse_state.time_between_wheel_movement = TIME_BETWEEN_WHEEL_MOVEMENT_2;
        mouse_speed_buttons_pressed++;
      } else {
        mouse_speed_buttons_pressed--;
      }
      break;
    case KC_GW2_DODGEJUMP:
      if (record->event.pressed) {
        register_code(KC_SPACE);
        register_code(KC_V);
        unregister_code(KC_SPACE);
        unregister_code(KC_V);
      }
      break;
  }

  // reset mouse speed to default if no speed button is pressed
  if (mouse_speed_buttons_pressed == 0) {
    mouse_state.time_between_movement = TIME_BETWEEN_MOVEMENT_DEFAULT;
    mouse_state.time_between_wheel_movement = TIME_BETWEEN_WHEEL_MOVEMENT_DEFAULT;
  }

  return true;
}

// Runs just one time when the keyboard initializes.
void matrix_init_user() {
  // Linux unicode support
  set_unicode_input_mode(UNICODE_MODE_LINUX);
  //set_unicode_input_mode(UC_WINC);
  timer_init();
  timer_clear();
}

// Runs constantly in the background, in a loop.
void matrix_scan_user() {
  static uint32_t d3_last_kc2_time = 0;
  uint32_t now = timer_read32();
  if (spam_d3_kc2 && TIMER_DIFF_32(now, d3_last_kc2_time) >= 750) {
    d3_last_kc2_time = now;
    register_code(KC_2);
    unregister_code(KC_2);
  }

  //uint32_t mouse_timediff = TIMER_DIFF_32(now, mouse_state.last_update_time);
  report_mouse_t mouseReport = pointing_device_get_report();

  // mouse movement
  uint32_t movement_steps = 0;
  if (mouse_state.up || mouse_state.down || mouse_state.left || mouse_state.right) {
    uint32_t time_diff_movement = TIMER_DIFF_32(now, mouse_state.last_movement_time);
    movement_steps = (time_diff_movement * 1000l) / mouse_state.time_between_movement;
    mouse_state.last_movement_time += (movement_steps * mouse_state.time_between_movement) / 1000l;
  }
  if (movement_steps > 127) movement_steps = 127;
  if (mouse_state.up) {
    mouseReport.y = -movement_steps;
  } else if (mouse_state.down) {
    mouseReport.y = movement_steps;
  }
  if (mouse_state.left) {
    mouseReport.x = -movement_steps;
  } else if (mouse_state.right) {
    mouseReport.x = movement_steps;
  }
  // mouse wheel movement
  uint32_t wheel_movement_steps = 0;
  if (mouse_state.wheel_up || mouse_state.wheel_down || mouse_state.wheel_left || mouse_state.wheel_right) {
    uint32_t time_diff_wheel_movement = TIMER_DIFF_32(now, mouse_state.last_wheel_movement_time);
    wheel_movement_steps = (time_diff_wheel_movement * 1000l) / mouse_state.time_between_wheel_movement;
    mouse_state.last_wheel_movement_time += (wheel_movement_steps * mouse_state.time_between_wheel_movement) / 1000l;
  }
  if (wheel_movement_steps > 127) wheel_movement_steps = 127;
  if (mouse_state.wheel_up) {
    mouseReport.v = wheel_movement_steps;
  } else if (mouse_state.wheel_down) {
    mouseReport.v = -wheel_movement_steps;
  }
  if (mouse_state.wheel_left) {
    mouseReport.h = -wheel_movement_steps;
  } else if (mouse_state.wheel_right) {
    mouseReport.h = wheel_movement_steps;
  }
  // buttons
  if (mouse_state.button_left) {
    mouseReport.buttons |= MOUSE_BTN2;
  } else {
    mouseReport.buttons &= ~MOUSE_BTN2;
  }
  if (mouse_state.button_right) {
    mouseReport.buttons |= MOUSE_BTN1;
  } else {
    mouseReport.buttons &= ~MOUSE_BTN1;
  }
  if (mouse_state.button_middle) {
    mouseReport.buttons |= MOUSE_BTN3;
  } else {
    mouseReport.buttons &= ~MOUSE_BTN3;
  }

  pointing_device_set_report(mouseReport);
}

// Runs whenever there is a layer state change.
layer_state_t layer_state_set_user(layer_state_t state) {
  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();

  uint8_t layer = biton32(state);
  switch (layer) {
    case BASE:
      if (macro_used) {
        clear_mods();
        macro_used = false;
      }
      break;
    case 1:
      ergodox_right_led_1_on();
      break;
    case 2:
      ergodox_right_led_2_on();
      break;
    case 3:
      ergodox_right_led_3_on();
      break;
    case 4:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      break;
    case 5:
      ergodox_right_led_1_on();
      ergodox_right_led_3_on();
      break;
    case 6:
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
    case 7:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
  }

  return state;
}
